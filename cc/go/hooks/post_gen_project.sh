#!/bin/sh

git init
git remote add origin git@gitlab.com:{{cookiecutter.repo_group}}/{{cookiecutter.repo_name}}.git
go mod init gitlab.com/{{cookiecutter.repo_group}}/{{cookiecutter.repo_name}}
make .gitignore .golangci.yml
git add -A
git commit -m "Initial commit"
